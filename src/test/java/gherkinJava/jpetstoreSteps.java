package gherkinJava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetstoreSteps {
	
	WebDriver driver;


	@Given("j'accede a l'URL de JpetStore")
	public void j_accede_a_l_url_de_jpetstore() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}
	
	@Given("Je clique sur le bouton Login")
	public void je_clique_sur_le_bouton_login() throws Throwable {
	    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/a[2]")).click();
	}

	@Given("Je saisis dans le champ username {string}")
	public void je_saisis_dans_le_champ_username(String username) {
		driver.findElement(By.name("username")).click();
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(username);
	}

	@Given("Je saisis dans le champs password {string}")
	public void je_saisis_dans_le_champs_password(String password) {
		 driver.findElement(By.name("password")).click();
		 driver.findElement(By.name("password")).clear();
		 driver.findElement(By.name("password")).sendKeys(password);
	}

	@Given("Je clique sur le bouton Login2")
	public void je_clique_sur_le_bouton_Login2() {
		 driver.findElement(By.name("signon")).click();
	}

	@Then("Je suis connecte")
	public void je_suis_connecte() {
		assertEquals(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/a[2]")).getText(),"Sign Out");
	}

	@Then("Le message accueil suivant est lisible {string}")
	public void le_message_accueil_suivant_est_lisible(String message) {
		assertEquals(driver.findElement(By.id("WelcomeContent")).getText(),message);
		driver.quit();
	}
}