package gherkinJava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class curaSteps {

	WebDriver driver;
	
	String loginHeaderXPath = "/html/body/section/div/div/div[1]/h2";
	String loginHeaderExpected = "Login";
	String usernameFieldId = "txt-username";
	String passwordFieldId = "txt-password";
	String loginButtonId = "btn-login";
	String username = "John Doe";
	String password = "ThisIsNotAPassword";
	
	String appointmentHeaderXPath = "/html/body/section/div/div/div/h2";
	String appointmentHeaderExpected = "Make Appointment";
	
	@Given("L'utilisateur est sur la page d'accueil")
	public void l_utilisateur_est_sur_la_page_d_accueil() {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://katalon-demo-cura.herokuapp.com/");
	}

	@When("L'utilisateur souhaite prendre un rendez-vous")
	public void l_utilisateur_souhaite_prendre_un_rendez_vous() {
		driver.findElement(By.id("btn-make-appointment")).click();
	}

	@Then("La page de connexion s'affiche")
	public void la_page_de_connexion_s_affiche() {
		assertEquals(driver.findElement(By.xpath(loginHeaderXPath)).getText(),loginHeaderExpected);
	}

	@When("L'utilisateur se connecte")
	public void l_utilisateur_se_connecte() {
		
		driver.findElement(By.id(usernameFieldId)).clear();
		driver.findElement(By.id(usernameFieldId)).sendKeys(username);
		
		driver.findElement(By.id(passwordFieldId)).clear();
		driver.findElement(By.id(passwordFieldId)).sendKeys(password);
		
		driver.findElement(By.id(loginButtonId)).click();
	}

	@Then("L'utilisateur est connecté sur la page de rendez-vous")
	public void l_utilisateur_est_connecté_sur_la_page_de_rendez_vous() {
		assertEquals(driver.findElement(By.xpath(appointmentHeaderXPath)).getText(),appointmentHeaderExpected);
	}
}
