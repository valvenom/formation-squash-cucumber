Feature: Connexion à l'application

Scenario: Connexion à l'application
  Given L'utilisateur est sur la page d'accueil
	When L'utilisateur souhaite prendre un rendez-vous
	Then La page de connexion s'affiche
	When L'utilisateur se connecte
	Then L'utilisateur est connecté sur la page de rendez-vous
